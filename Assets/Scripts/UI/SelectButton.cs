using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using PlayfabManagerName;
using System;

public class SelectButton : MonoBehaviour
{
    [Header("References")]
    public TextMeshProUGUI ButtonText;
    public Button ButtonSelect;
    public Button ButtonBuy;
    [Header("String")]
    public string SelectedString = "Selected";
    public string ToSelect = "Select";

    private int _tabIndex = 1;
    private Material _currentMaterial;
    private GameObject _currentFX;
    private string _currentID;

    private void Awake()
    {
        ButtonSelect.gameObject.SetActive(false);
        ButtonBuy.gameObject.SetActive(false);
        PlayfabManager.OnGetInventory += ResetButton;
    }

    private void OnDestroy()
    {
        PlayfabManager.OnGetInventory -= ResetButton;
    }

    private void ResetButton()
    {
        Debug.Log("Hey, button reseteado");
        if (_tabIndex == 4)
            ChangeFX(_currentFX);
        else
            ChangeMat(_currentMaterial);
    }
    public void ChangeTab(int index) { _tabIndex = index; }

    public void ChangeFX(GameObject VFX)
    {
        if(_tabIndex == 4)
        {
            if (PlayfabManager.Instance.CheckInventory(_currentID))
            {
                SetButtonSelect(true);
                ButtonSelect.gameObject.SetActive(true);
                _currentFX = VFX;
            }
            else
            {
                SetButtonSelect(false);
            }
        }
    }

    public void ChangeMat(Material Mat)
    {
        ButtonSelect.gameObject.SetActive(true);

        if (_tabIndex == 1)
        {
            if(SkinsManager.Instance.CheckWhiteMat(Mat)) // Check with Playfab
            {
                BlockButton();
            }
            else
            {
                PreSelectMaterial(Mat);
            }
        }
        else if (_tabIndex == 2)
        {
            if (SkinsManager.Instance.CheckBlackMat(Mat)) // Check with Playfab
            {
                BlockButton();
            }
            else
            {
                PreSelectMaterial(Mat);
            }
        }
        else if (_tabIndex == 3)
        {
            if (SkinsManager.Instance.CheckBoardMat(Mat)) // Check with Playfab
            {
                BlockButton();
            }
            else
            {
                PreSelectMaterial(Mat);
            }
        }
    }

    public void SetStringID(string ID)
    {
        _currentID = ID;
    }

    private void PreSelectMaterial(Material Mat)
    {
        if (PlayfabManager.Instance.CheckInventory(_currentID))
        {
            
            SetButtonSelect(true);
            ButtonText.text = ToSelect;
            ButtonSelect.interactable = true;
            _currentMaterial = Mat;
        }
        else
        {
            SetButtonSelect(false);
        }
    }
    

    private void BlockButton()
    {
        SetButtonSelect(true);
        ButtonText.text = SelectedString;
        ButtonSelect.interactable = false;
    }

    private void SetButtonSelect(bool value)
    {
        ButtonBuy.gameObject.SetActive(!value);
        ButtonSelect.gameObject.SetActive(value);
    }

    public void SendMaterial()
    {
        if (_currentMaterial == null && _tabIndex < 4) { return; }

        BlockButton();

        if (_tabIndex == 1)
        {
            SkinsManager.Instance.SetWhiteMat(_currentMaterial);
        }
        else if (_tabIndex == 2)
        {
            SkinsManager.Instance.SetBlackMat(_currentMaterial);
        }
        else if (_tabIndex == 3)
        {
            SkinsManager.Instance.SetBoardMat(_currentMaterial);
        }
        else if(_tabIndex == 4 && _currentFX)
        {
            SkinsManager.Instance.SetDestroyFX(_currentFX);
        }

        SkinsManager.Instance.Save();
    }

    public void BuyItem()
    {
        PlayfabManager.Instance.BuyItem(_currentID,
            (result) => {});
    }
}
