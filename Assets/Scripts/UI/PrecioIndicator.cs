using PlayfabManagerName;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PrecioIndicator : MonoBehaviour
{
    private TextMeshProUGUI _priceText;

    private void Awake()
    {
        _priceText = GetComponent<TextMeshProUGUI>();
        ChangePrice(1);
    }
    public void ChangePrice(int tabIndex)
    {
        switch(tabIndex)
        {
            case 1: case 2:
                _priceText.text = "x" + PlayfabManager.Instance.ServerShopRatio.PiecesCost.ToString();
                break;
            case 3:
                _priceText.text = "x" + PlayfabManager.Instance.ServerShopRatio.BoardCost.ToString();
                break;
            case 4:
                _priceText.text = "x" + PlayfabManager.Instance.ServerShopRatio.VFXCost.ToString();
                break;
        }
    }

}
