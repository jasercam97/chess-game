using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IndicatorsPlayfab : MonoBehaviour
{
    private Animator _anim;
    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }
    void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        switch(scene.buildIndex)
        {
            case 0: // lobby
                _anim.SetTrigger("Lobby");
                break;
            case 1: // Game
                _anim.SetTrigger("Game");
                break;
            case 2: // Shop
                _anim.SetTrigger("Shop");
                break;
        }
    }

    public void DisappearOnPlay()
    {
        _anim.SetTrigger("Game");
    }
}
