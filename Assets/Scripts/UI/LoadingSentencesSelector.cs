using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadingSentencesSelector : MonoBehaviour
{
    public List<string> Frases;

    private TextMeshProUGUI _text;
    void Start()
    {
        _text = GetComponent<TextMeshProUGUI>();
        InvokeRepeating(nameof(ChangeText), 1.5f, 5f);
    }

    private void ChangeText()
    {
        _text.text = Frases[(int)Random.Range(0, Frases.Count)];
    }
}
