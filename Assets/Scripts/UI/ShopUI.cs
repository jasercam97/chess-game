using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    public GameObject Whites, Blacks, Boards, Effects;

    public SelectButton ButtonToSelect;
    void Start()
    {
        Whites.SetActive(true);
        Blacks.SetActive(false);
        Boards.SetActive(false);
        Effects.SetActive(false);
    }

    public void DeactivateAll(int index)
    {
        Whites.SetActive(false);
        Blacks.SetActive(false);
        Boards.SetActive(false);
        Effects.SetActive(false);

        ButtonToSelect.ChangeTab(index);

        if (index == 1)
        {
            Whites.SetActive(true);
        }
        else if (index == 2)
        {
            Blacks.SetActive(true);
        }
        else if (index == 3)
        {
            Boards.SetActive(true);
        }
        else if (index == 4)
        {
            Effects.SetActive(true);
        }

        else Debug.LogError("Index of Shop Tab out of Boundaries");

    }
}
