using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using UnityEngine.Events;
using TMPro;

namespace PlayfabManagerName
{
    public class PlayfabManager : Singleton<PlayfabManager>
    {
        public static event Action OnGetInventory;

        [Header("Playfab Variables")]
        public string GameVersion;
        public ShopRatio ServerShopRatio = new ShopRatio();
        public List<CatalogItem> CatalogItems;
        public List<ItemInstance> Inventory;

        [Header("References")]
        public TextMeshProUGUI MoneyText;
        public TextMeshProUGUI RankingText;
        public GameObject LoadingPanel;

        [Header("Control Versiones")]
        [SerializeField]
        private bool _isTest = false;

        private static string PLAYFAB_PROJECTID_TEST = "9EE97";
        private static string PLAYFAB_PROJECTID_RELEASE = "47872";

        private bool _isItemPurchased;

        private int _chessPoints;

        private string _playfabID;

        private void Awake()
        {
            SetupPlayfabServer();
            LoadingPanel.SetActive(true);
        }

        void Start()
        {
            ServerLogin();
        }

        #region PlayFab Server

        private void SetupPlayfabServer()
        {
            PlayFabSettings.TitleId = (_isTest ? PLAYFAB_PROJECTID_TEST : PLAYFAB_PROJECTID_RELEASE);
        }

        public void LoadServerData()
        {
            PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
            result => {
                LoadGameSetup(result.Data);
            },
            error => {
                Debug.Log("Getting titleData ERROR::" + error.ErrorMessage);
            }
    );
        }

        private void LoadGameSetup(Dictionary<string, string> data)
        {
            SetPlayfabVersion(data["ClientVersion"]);
            SetPlayfabShopRatio(data["ShopRatio"]);
        }

        private void SetPlayfabVersion(string version)
        {
            GameVersion = version;
        }

        private void SetPlayfabShopRatio(string ratioJson)
        {
            JsonUtility.FromJsonOverwrite(ratioJson, ServerShopRatio);
        }
        #endregion

        #region LOG IN
        private void ServerLogin()
        {
            Login(OnLoginSucces, OnLoginFailed);
        }
    
        private void Login(Action<LoginResult> onSuccess, Action<PlayFabError> onFail)
        {
            var request = new LoginWithCustomIDRequest
            {
                CreateAccount = true,
                CustomId = SystemInfo.deviceUniqueIdentifier
            };

            PlayFabClientAPI.LoginWithCustomID(request, onSuccess, onFail);
        }

        private void OnLoginSucces(LoginResult loginResult)
        {
            Debug.Log("User: " + loginResult.PlayFabId);
            _playfabID = loginResult.PlayFabId;
            LoadServerData();
            LoadCatalog();
            LoadInventory();
            GetLeaderboard();

            AddPoints(0);

            //LoadingPanel.SetActive(false);
            if(LoadingPanel) Destroy(LoadingPanel, 2f);
        }

        private void OnLoginFailed(PlayFabError error)
        {
            Debug.LogError("Login failed: ERROR:: " + error.ErrorMessage);
        }

        #endregion

        #region SHOP

        private void LoadCatalog()
        {
            GetCatalogItems(
                (catalog) =>
                {
                    CatalogItems = catalog;
                });
        }

        private void GetCatalogItems(Action<List<CatalogItem>> onSuccess, Action onError = null)
        {
            var request = new GetCatalogItemsRequest()
            {
                CatalogVersion = "Shop1"
            };

            PlayFabClientAPI.GetCatalogItems(request,
                (result) =>
                {
                    onSuccess(result.Catalog);
                },
                (error) =>
                {
                    onError();
                });
        }

        public void BuyItem(string ID, Action<bool> onSuccess)
        {
            CatalogItem buyingItem = null;
            foreach(CatalogItem item in CatalogItems)
            {
                if (item.ItemId == ID)
                    buyingItem = item;
            }
            if (buyingItem != null) PurchaseItem(buyingItem, 
                (item) =>
                {
                    onSuccess(true);
                    LoadInventory();
                });
        }

        private void PurchaseItem(CatalogItem item, Action<ItemInstance> onSuccess, Action onError = null)
        {
            var request = new PurchaseItemRequest()
            {
                CatalogVersion = "Shop1",
                VirtualCurrency = "CP",
                Price = (int)item.VirtualCurrencyPrices["CP"],
                ItemId = item.ItemId,
            };

            PlayFabClientAPI.PurchaseItem(request,
                (result) =>
                {
                    onSuccess(result.Items[0]);
                },
                (error) =>
                {
                    onError?.Invoke();
                });
        }
        #endregion

        #region INVENTORY

        public bool CheckInventory(string ID)
        {
            _isItemPurchased = false;
            foreach (ItemInstance item in Inventory)
            {
                if (item.ItemId == ID)
                    _isItemPurchased = true;
            }

            return _isItemPurchased;
        }

        public void LoadInventory()
        {
            Inventory.Clear();
            Debug.Log("Inventory Loaded");
            GetInventory(
                (inventory) =>
                {
                    Inventory = inventory.Inventory;
                    _chessPoints = inventory.VirtualCurrency["CP"];
                    UpdateMoneyDisplay();
                    if(OnGetInventory != null)
                        OnGetInventory.Invoke();
                });
        }

        private void GetInventory(Action<GetUserInventoryResult> onSuccess, Action onError = null)
        {
            var request = new GetUserInventoryRequest()
            {

            };

            PlayFabClientAPI.GetUserInventory(request,
                (result) =>
                {
                    onSuccess(result);
                },
                (error) =>
                {
                    onError?.Invoke();
                });
        }
        #endregion

        #region MONEY
        public void EarnMoney(int amount)
        {
            Debug.Log("Y mi dinero scarface?");
            AddMoney(amount,
                (result) =>
                {
                    Debug.Log(result);
                    _chessPoints = result;
                    UpdateMoneyDisplay();
                },
                (error) =>
                {
                    Debug.Log("ERROR:: " + error.ErrorMessage);
                });
        }
        private void AddMoney(int amount, Action<int> onSucess, Action<PlayFabError> onError = null)
        {
            var request = new AddUserVirtualCurrencyRequest()
            {
                Amount = amount,
                VirtualCurrency = "CP",

            };

            PlayFabClientAPI.AddUserVirtualCurrency(request,
                (result) =>
                {
                    onSucess(result.Balance);
                }, onError);
        }
        private void UpdateMoneyDisplay()
        {
            MoneyText.text = _chessPoints.ToString() ;
        }

        #endregion

        #region LEADERBOARD

        public void AddPoints(int amount)
        {
            var request = new UpdatePlayerStatisticsRequest()
            {
                Statistics = new List<StatisticUpdate>()
                {
                    new StatisticUpdate()
                    {
                        StatisticName = "MMR",
                        Value = amount,
                    }
                }
            };

            PlayFabClientAPI.UpdatePlayerStatistics(request,
                (result) =>
                {
                    Debug.Log("MMR Updated");
                    GetLeaderboard();
                },
                (error) =>
                {
                    Debug.LogError("ERROR:: " + error.ErrorMessage);
                });
        }

        private void GetLeaderboard()
        {
            var request = new GetLeaderboardRequest()
            {
                StatisticName = "MMR",

            };

            PlayFabClientAPI.GetLeaderboard(request,
                (result) =>
                {
                    foreach(PlayerLeaderboardEntry player in result.Leaderboard)
                    {
                        if (player.PlayFabId == _playfabID)
                        {
                            RankingText.text = (player.Position + 1).ToString();
                        }
                    }
                },
                (error) =>
                {
                    Debug.LogError("ERROR:: " + error.ErrorMessage);
                });
        }

        #endregion
    }

}

public class ShopRatio
{
    public int PiecesCost;
    public int VFXCost;
    public int BoardCost;
}
