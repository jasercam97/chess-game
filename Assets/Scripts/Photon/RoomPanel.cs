using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using Utils;

public class RoomPanel : MonoBehaviourPunCallbacks
{
    [SerializeField] private TextMeshProUGUI _roomNameText; 
    [SerializeField] private TextMeshProUGUI _playerCountText;

    [SerializeField] private TextMeshProUGUI _player1NameText;
    [SerializeField] private TextMeshProUGUI _player2NameText;
    [SerializeField] private Button _whitePlayerButton;
    [SerializeField] private Button _blackPlayerButton;

    [SerializeField] private Button _playButton;

    private bool _teamChosen = false;

    public void Show(bool v)
    {
        gameObject.SetActive(v);

        if (v) { Init(); }
    }

    private void Init()
    {
        UpdateRoomName();
        UpdatePlayersInfo();
    }

    private void UpdatePlayersInfo()
    {
        UpdatePlayerCount();
        SetPlayerInfo();
        SetupPlayButton();
    }

    private void SetupPlayButton()
    {
        _playButton.interactable = (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount == 2 && _teamChosen);
    }

    private void UpdatePlayerCount()
    {
        int playerCount = PhotonNetwork.CurrentRoom.PlayerCount;
        _playerCountText.text = "Players: " + playerCount.ToString();
    }

    private void UpdateRoomName()
    {
        _roomNameText.text = PhotonNetwork.CurrentRoom.GetPlayer(1).NickName + "'s ROOM";
    }

    private void SetPlayerInfo()
    {
        if(PhotonNetwork.LocalPlayer == PhotonNetwork.CurrentRoom.GetPlayer(1))
        {
            SetPlayer1Info();
        }
        if(PhotonNetwork.LocalPlayer == PhotonNetwork.CurrentRoom.GetPlayer(2)) 
        {
            SetPlayer1Info();
            SetPlayer2Info();
        }
    }

    void SetPlayer1Info()
    {
        _player1NameText.gameObject.SetActive(true);
        _player1NameText.text = PhotonNetwork.CurrentRoom.GetPlayer(1).NickName;
    }

    void SetPlayer2Info()
    {
        _player2NameText.gameObject.SetActive(true);
        _player2NameText.text = PhotonNetwork.CurrentRoom.GetPlayer(2).NickName;

        if (PhotonNetwork.LocalPlayer == PhotonNetwork.CurrentRoom.GetPlayer(2))
        {
            _whitePlayerButton.interactable = false;
            _blackPlayerButton.interactable = false;
        }
    }

    public void OnPlayButtonPressed()
    {
        NetworkManager.Instance.LoadGame();
    }

    public void SelectTeam(int team)
    {
        NetworkManager.Instance.SetPlayerTeam(team);
        _teamChosen = true;

        if (PhotonNetwork.CurrentRoom.GetPlayer(1) == PhotonNetwork.LocalPlayer)
        {
            if (team == 0)
            {
                _blackPlayerButton.interactable = false;
                _whitePlayerButton.interactable = true;
            }
            if (team == 1) 
            { 
                _blackPlayerButton.interactable = true;
                _whitePlayerButton.interactable = false;
            }
        }
        SetupPlayButton();
    }

    public void RestrictTeamChoice(TeamColor occupiedTeam)
    {
        //var teamToChoose = occupiedTeam == TeamColor.White ? _whitePlayerButton : _blackPlayerButton;

        if(occupiedTeam == TeamColor.Black) { SelectTeam(1); }
        else if(occupiedTeam == TeamColor.White) { SelectTeam(0); }        
    }

    #region PUN Callbacks

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        Debug.Log("Player " + newPlayer.NickName + " joined");
        UpdatePlayersInfo();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        Debug.Log("Player " + otherPlayer.NickName + " left");
        UpdatePlayersInfo();
    }

    public override void OnMasterClientSwitched(Photon.Realtime.Player newMasterClient)
    {
        Debug.Log(newMasterClient + " is the new host");
        SetupPlayButton();
    }

    #endregion
}
