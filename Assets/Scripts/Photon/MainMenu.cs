using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject _startPanel;
    [SerializeField] private GameObject _gameModePanel;
    [SerializeField] private GameObject _loginPanel;
    [SerializeField] private GameObject _lobbyPanel;

    [SerializeField] private Button _quickPlayButton;
    [SerializeField] private RoomPanel _roomPanel;

    private void Awake()
    {
        ShowStart(true);
        ShowLogin(false);
        ShowLobby(false);
    }

    private void ShowStart(bool v)
    {
        _startPanel.SetActive(v);
    }

    private void ShowGameMode(bool v)
    {
        _gameModePanel.SetActive(v);
    }

    private void ShowLobby(bool v)
    {
        _lobbyPanel.SetActive(v);
    }

    private void ShowLogin(bool v)
    {
        _loginPanel.SetActive(v);
    }

    private void ShowSettings(bool v)
    {
        //Settings
    }

    private void ShowQuickButton(bool v)
    {
        _quickPlayButton.gameObject.SetActive(v);
    }

    private void ShowRoom(bool v)
    {
        _roomPanel.Show(v);
        ShowQuickButton(!v);
    }

    #region Buttons

    public void PlayButtonPressed()
    {
        ShowStart(false);
        ShowGameMode(true);
    }

    public void PlayButtonBack()
    {
        ShowStart(true);
        ShowGameMode(false);
    }

    public void SettingsButtonPressed()
    {
        Debug.Log("Settings");
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }

    public void LocalButtonPressed(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void OnlineButtonPressed()
    {
        ShowGameMode(false);
        ShowLogin(true);
    }

    public void LoginButtonPressed()
    {
        ShowLogin(false);
        PhotonUIManager.Instance.ShowLoadingScreen(true);
        NetworkManager.Instance.Login();
    }

    public void QuickPlayButtonPressed()
    {
        ShowQuickButton(false);
        PhotonUIManager.Instance.ShowLoadingScreen(true);
        NetworkManager.Instance.QuickPlay();
    }

    #endregion

    #region PUN Callbacks

    public override void OnConnectedToMaster()
    {
        ShowLobby(true);
        ShowLogin(false);
        PhotonUIManager.Instance.ShowLoadingScreen(false);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        ShowLobby(false);
        ShowLogin(true);
    }

    public override void OnJoinedRoom()
    {
        PhotonUIManager.Instance.ShowLoadingScreen(false);
        ShowRoom(true);
    }

    public override void OnLeftRoom()
    {
        ShowRoom(false);
    }

    #endregion
}