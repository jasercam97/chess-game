using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Photon.Pun;

public class PlayerNameInputField : MonoBehaviour
{
    private TMP_InputField _inputField;

    private void Awake()
    {
        _inputField = GetComponent<TMP_InputField>();
    }

    private void Start()
    {
        LoadLastPlayerName();
    }

    private void LoadLastPlayerName()
    {
        string playerName = LoadPlayerName();

        PhotonNetwork.NickName = playerName;
        _inputField.text = playerName;
    }

    private void SaveCurrentName(string name)
    {
        PlayerPrefs.SetString("PlayerName", name);
        PlayerPrefs.Save();
    }

    private string LoadPlayerName()
    {
        return PlayerPrefs.GetString("PlayerName", "");
    }

    public void SetPlayerName()
    {
        string name = _inputField.text;

        if (!string.IsNullOrEmpty(name))
        {
            PhotonNetwork.NickName = name;
            SaveCurrentName(name);
        }
        else { Debug.LogError("Invalid name"); }
    }
}
