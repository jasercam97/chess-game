using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class PhotonUIManager : Singleton<PhotonUIManager>
{
    [SerializeField] private GameObject _loadingScreen;

    public void ShowLoadingScreen(bool v)
    {
        _loadingScreen.SetActive(v);
        Debug.Log("Loading screen state: " + v.ToString());
    }
}
