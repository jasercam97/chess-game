using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
public class NetworkManager : NetworkSingleton<NetworkManager>
{
    [SerializeField] private byte maxPlayerPerRoom = 2;

    [SerializeField] private RoomPanel roomPanel;

    private const string TEAM = "team";

    private string _gameVersion = "1";

    public OnlineModeController _onlineModeController;

    private GameInitializer _init;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void SetDependencies(OnlineModeController onlineModeController)
    {
        this._onlineModeController = onlineModeController;
    }

    public void Login()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = _gameVersion;
        }
    }

    public void QuickPlay()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            Debug.LogError("[Network Manager]: Not connected");
            Login();
        }
    }

    public void LoadGame()
    {
        if(PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;

            Debug.Log("[Network Manager]: Loading game");
            PhotonNetwork.LoadLevel(1);
        }
    }

    public void SetPlayerTeam(int team)
    {
        PhotonNetwork.LocalPlayer.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() { { TEAM, team } });
        _init.SetTeam((TeamColor)team);
    }

    private void PrepareTeamSelectionOptions()
    {
        Debug.Log("Prepare: " + PhotonNetwork.CurrentRoom.PlayerCount + " players");
        if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
        {
            var player = PhotonNetwork.CurrentRoom.GetPlayer(1);
            if (player.CustomProperties.ContainsKey(TEAM))
            {
                var occupiedTeam = player.CustomProperties[TEAM];
                roomPanel.RestrictTeamChoice((TeamColor)occupiedTeam);
            }
        }
    }

    public void SetInit(GameInitializer init)
    {
        _init = init;
    }


    #region PUN Callbacks

    public override void OnConnectedToMaster()
    {
        Debug.Log("[Network Manager]: Connected to " + PhotonNetwork.CloudRegion + " server");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("[Network Manager]: Disconnected with reason " + cause);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayerPerRoom });
    }

    public override void OnJoinedRoom()
    {
        PrepareTeamSelectionOptions();
        Debug.Log("[Network Manager]: Joined room " + PhotonNetwork.CurrentRoom.Name);
    }

    #endregion
}
