using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LogManager : MonoBehaviour
{
    public static LogManager Instance;
    public GameObject LogHUD;
    public RectTransform ContentLog;
    [Header("Prefabs")]
    public GameObject PrefabText;
    public List<LogClass> Log;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        Log = new List<LogClass>();
    }
    void Start()
    {
        ShowHUD(false);
        ResetLog();
    }

    public void ShowHUD(bool state)
    {
        LogHUD.SetActive(state);
    }

    public void AlternateLogHUD()
    {
        ShowHUD(!LogHUD.activeInHierarchy);
    }
    public void ResetLog()
    {
        Log.Clear();
    }

    public void AddLogMovement(LogClass logMov)
    {
        Log.Add(logMov);
        GameObject LogMsg = Instantiate(PrefabText, ContentLog);
        LogMsg.GetComponent<TextMeshProUGUI>().text = "<b>" + logMov.MovingPiece.Team.ToString() + ":</b> " + logMov.MovingPiece.GetType().Name + " - went from: " + logMov.StartPos + " to " + logMov.EndPos;
        //Debug.Log(logMov.MovingPiece.GetType().Name + logMov.MovingPiece.Team.ToString() + "went from: " + logMov.StartPos + " to " + logMov.EndPos);
    }




}
