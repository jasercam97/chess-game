using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Camera MainCamera;
    protected Plane Plane;

    public bool Rotate = true;

    Vector3 _delta1;
    Vector3 _delta2;

    Vector3 pos1;
    Vector3 pos2;
    Vector3 pos1b;
    Vector3 pos2b;

    void Start()
    {
        if (MainCamera == null)
        {
            MainCamera = Camera.main;
        }             
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount >= 1)
            Plane.SetNormalAndPosition(transform.up, transform.position);

        _delta1 = Vector3.zero;
        _delta2 = Vector3.zero;

        //Scroll - Pan
        ScrollCheck();

        //Pinch
        ZoomCheck();

        //Rotation
        if (Rotate && pos2b != pos2)
            MainCamera.transform.RotateAround(transform.position, Plane.normal, Vector3.SignedAngle(pos2 - pos1, pos2b - pos1b, Plane.normal));

    }

    private void ZoomCheck()
    {
        if (Input.touchCount >= 2)
        {
            pos1 = PlanePosition(Input.GetTouch(0).position);
            pos2 = PlanePosition(Input.GetTouch(1).position);
            pos1b = PlanePosition(Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition);
            pos2b = PlanePosition(Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition);

            float zoom = Vector3.Distance(pos1, pos2) / Vector3.Distance(pos1b, pos2b);

            if (zoom == 0 || zoom > 10)
                return;

            MainCamera.transform.position = Vector3.LerpUnclamped(transform.position, MainCamera.transform.position, 1 / zoom);
        }
    }

    private void ScrollCheck()
    {
        if (Input.touchCount >= 1)
        {
            _delta1 = PlanePositionDelta(Input.GetTouch(0));
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
                MainCamera.transform.Translate(_delta1, Space.World);
        }
    }

    protected Vector3 PlanePosition(Vector2 screenPos)
    {
        Ray rayNow = MainCamera.ScreenPointToRay(screenPos);
        if (Plane.Raycast(rayNow, out var enterNow))
            return rayNow.GetPoint(enterNow);

        return Vector3.zero;
    }

    protected Vector3 PlanePositionDelta(Touch touch)
    {
        if (touch.phase != TouchPhase.Moved)
            return Vector3.zero;

        Ray rayBefore = MainCamera.ScreenPointToRay(touch.position - touch.deltaPosition);
        Ray rayNow = MainCamera.ScreenPointToRay(touch.position);
        if (Plane.Raycast(rayBefore, out var enterBefore) && Plane.Raycast(rayNow, out var enterNow))
            return rayBefore.GetPoint(enterBefore) - rayNow.GetPoint(enterNow);

        return Vector3.zero;
    }

}
