using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject CameraTop, CameraSided, FreeCamera;
   

    private void DisableAll()
    {
        CameraTop.SetActive(false);
        CameraSided.SetActive(false);
        FreeCamera.SetActive(false);
    }

    public void Sided()
    {
        DisableAll();
        CameraSided.SetActive(true);
    }

    public void Top()
    {
        DisableAll();
        CameraTop.SetActive(true);
    }

    public void Free()
    {
        DisableAll();
        FreeCamera.SetActive(true);
    }
}
