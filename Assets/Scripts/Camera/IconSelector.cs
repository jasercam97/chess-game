using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconSelector : MonoBehaviour
{
    public Sprite WhiteSprite;
    public Sprite BlackSprite;

    private SpriteRenderer _sr;

    void Awake()
    {
        _sr = GetComponent<SpriteRenderer>();
    }

    public void CheckIcon(TeamColor team)
    {
        if (!_sr) return;

        if (team == TeamColor.Black)
        {
            _sr.sprite = BlackSprite;
        }
        else
        {
            _sr.sprite = WhiteSprite;
            transform.Rotate(0f, 0f, 180f);
        }
    }
}
