using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopCamera : MonoBehaviour
{
    [Header("Parameters")]
    public float Speed = 3f;
    [Header("References")]
    public SelectButton ButtonToSelect;
    [Header("Pieces")]
    public MeshRenderer Piece;
    [Header("Board")]
    public MeshRenderer Board;
    [Header("Effects")]
    public Transform SpawnPosEffect;
    public Transform SpawnPosEffect2;


    void Update()
    {
        transform.Rotate(new Vector3(0, 1f, 0) * Speed);
    }

    
    public void SetPieceMaterial(Material Mat)
    {
        Piece.material = Mat;
        ButtonToSelect.ChangeMat(Mat);
    }

    public void SetBoardMaterial(Material Mat)
    {
        Board.material = Mat;
        ButtonToSelect.ChangeMat(Mat);
    }

    public void ShowEffect(GameObject VFX)
    {
        Instantiate(VFX, SpawnPosEffect.position, SpawnPosEffect.rotation);
        Instantiate(VFX, SpawnPosEffect2.position, SpawnPosEffect2.rotation);
        ButtonToSelect.ChangeFX(VFX);
    }


}
