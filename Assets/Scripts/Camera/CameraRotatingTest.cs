using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotatingTest : MonoBehaviour
{
    public List<Material> MaterialsToTest;
    public List<MeshRenderer> Piezas;
    public List<GameObject> Particles;
    public float speedRotating = 15f;
    public float TimeBetweenMats = 0.5f;
    public Transform SpawnPos;


    int _index = 0;
    int _ind = 0;
    void Start()
    {
        InvokeRepeating(nameof(ChangeMat), 0, TimeBetweenMats);
        InvokeRepeating(nameof(SpawnPar), 0, TimeBetweenMats);

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 1, 0) * speedRotating * Time.deltaTime);
    }

    private void ChangeMat()
    {
        foreach(MeshRenderer rend in Piezas)
        {
            rend.material = MaterialsToTest[_index];
        }
        _index++;
    }

    private void SpawnPar()
    {
        var go = Instantiate(Particles[_ind], SpawnPos.position, SpawnPos.rotation);
        go.transform.localScale = Vector3.one * 0.1f;
        Destroy(go, TimeBetweenMats);
        _ind++;
    }
}
