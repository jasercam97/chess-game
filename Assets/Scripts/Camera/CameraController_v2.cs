using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_v2 : MonoBehaviour
{
    public Camera MainCamera;
    public float rotSpeed = 50f;
    protected Plane Plane;
    public float MinZCamera = -10f;
    public float MaxZCamera = -4f;

    public bool Rotate = true;

    [Range(-10, 45)]
    private float _rotX;
    
    private const float ZoomSensibility = 20f;
    
    void Start()
    {
        if (MainCamera == null)
        {
            MainCamera = Camera.main;
        }             
    }

    // Update is called once per frame
    void Update()
    {
        //Scroll - Rotation
        ScrollCheck();

        //Pinch
        ZoomCheck();

    }

    private void ZoomCheck()
    {
        if (Input.touchCount >= 2)
        {

            Vector3 _finger1 = Input.GetTouch(0).position;
            Vector3 p_finger1 = Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition;
            Vector3 _finger2 = Input.GetTouch(1).position;
            Vector3 p_finger2 = Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition;



            float zoomRaw = Vector3.Distance(_finger1, _finger2) / Vector3.Distance(p_finger1, p_finger2);

            if (zoomRaw < 0.99f)
                MainCamera.transform.Translate(new Vector3(0f, 0f, Time.deltaTime * -ZoomSensibility), Space.Self);
            else if (zoomRaw > 1.01f)
                MainCamera.transform.Translate(new Vector3(0f, 0f, Time.deltaTime * ZoomSensibility), Space.Self);

            Vector3 posCam = MainCamera.transform.localPosition;
            posCam.z = Mathf.Clamp(posCam.z, MinZCamera, MaxZCamera);
            MainCamera.transform.localPosition = posCam;

        }
    }

    private void ScrollCheck()
    {
        if (Input.touchCount >= 1)
        {
            Vector3 rotation = new Vector3(0f, Input.GetTouch(0).deltaPosition.x * rotSpeed, 0);
            transform.Rotate(rotation);
            //_rotX += Input.GetTouch(0).deltaPosition.y * rotSpeed;
            //Vector3 eulerAng = transform.rotation.eulerAngles;
            //eulerAng.x = _rotX;
            //transform.rotation = Quaternion.Euler(eulerAng);
        }
    }

}
