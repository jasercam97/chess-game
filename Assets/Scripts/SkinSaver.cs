using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSaver : MonoBehaviour
{
    // Player Prefs + Lista de Materiales. Se guarda el indice en la lista y se recupera al empezar el juego

    public List<Material> WhiteMaterials;
    public List<Material> BlackMaterials;
    public List<Material> BoardMaterials;
    public List<GameObject> DestroyFXs;

    private static string PP_WhiteMat = "WhiteMat";
    private static string PP_BlackMat = "BlackMat";
    private static string PP_BoardMat = "WhiteMat";
    private static string PP_DestroyFX = "DestroyFX";

    private void Start()
    {
        LoadMaterials();
    }

    public void SaveMaterials()
    {
        PlayerPrefs.SetInt(PP_WhiteMat, WhiteMaterials.IndexOf(SkinsManager.Instance.GetWhiteMat()));
        Debug.Log("Enviado:" + WhiteMaterials.IndexOf(SkinsManager.Instance.GetWhiteMat()));
        Debug.Log("Guardado:" + PlayerPrefs.GetInt(PP_WhiteMat));
        PlayerPrefs.SetInt(PP_BlackMat, BlackMaterials.IndexOf(SkinsManager.Instance.GetBlackMat()));
        PlayerPrefs.SetInt(PP_BoardMat, BoardMaterials.IndexOf(SkinsManager.Instance.GetBoardMat()));
        PlayerPrefs.SetInt(PP_DestroyFX, DestroyFXs.IndexOf(SkinsManager.Instance.GetDestroyFX()));
       
        PlayerPrefs.Save();

    }

    public void LoadMaterials()
    {
        SkinsManager.Instance.SetWhiteMat(WhiteMaterials[PlayerPrefs.GetInt(PP_WhiteMat)]);
        SkinsManager.Instance.SetBlackMat(BlackMaterials[PlayerPrefs.GetInt(PP_BlackMat)]);
        SkinsManager.Instance.SetBoardMat(BoardMaterials[PlayerPrefs.GetInt(PP_BoardMat)]);
        SkinsManager.Instance.SetDestroyFX(DestroyFXs[PlayerPrefs.GetInt(PP_DestroyFX)]);

    }
}
