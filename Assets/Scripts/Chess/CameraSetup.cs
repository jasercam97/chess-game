using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class CameraSetup : MonoBehaviour
{
    [SerializeField] private GameObject _mainCamera;

    //private void Awake()
    //{
    //    _mainCamera = GetComponent<Camera>();
    //}

    public void SetupCamera(TeamColor team)
    {
        if(team == TeamColor.Black) { FlipCamera(); }
    }

    public void FlipCamera()
    {
        _mainCamera.transform.position = new Vector3(
            _mainCamera.transform.position.x,
            _mainCamera.transform.position.y,
            -_mainCamera.transform.position.z);
        _mainCamera.transform.Rotate(Vector3.up, 180f, Space.World);
    }
}
