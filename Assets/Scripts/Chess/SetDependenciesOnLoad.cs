using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDependenciesOnLoad : MonoBehaviour
{
    [SerializeField] private Transform _anchor;
    [SerializeField] private CameraSetup _camera;
    [SerializeField] private ChessUIManager _UIManager;


    private GameInitializer _init;

    private void Awake()
    {
        _init = FindObjectOfType<GameInitializer>();   
    }

    private void Start()
    {
        SetDependencies();
        _init.StartGameMode();
    }

    void SetDependencies()
    {
        _init.SetDependencies(_UIManager, _anchor, _camera);
    }
}
