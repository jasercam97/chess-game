﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class King : Piece
{

    Vector2Int[] _directions = new Vector2Int[]
    {
        new Vector2Int(-1, 1),
        new Vector2Int(0, 1),
        new Vector2Int(1, 1),
        new Vector2Int(-1, 0),
        new Vector2Int(1, 0),
        new Vector2Int(-1, -1),
        new Vector2Int(0, -1),
        new Vector2Int(1, -1),
    };

    private Piece leftRook;
    private Piece rightRook;

    private Vector2Int leftCastlingMove;
    private Vector2Int rightCastlingMove;

    public override List<Vector2Int> SelectAvailableSquares()
    {
        AvailableMoves.Clear();
        AssignStandardMoves();
        AssignCastlingMoves();
        return AvailableMoves;
    }

    private void AssignCastlingMoves()
    {
        leftCastlingMove = new Vector2Int(-1, -1);
        rightCastlingMove = new Vector2Int(-1, -1);
        if (!HasMoved)
        {
            leftRook = GetPieceInDirection<Rook>(Team, Vector2Int.left);
            if (leftRook && !leftRook.HasMoved)
            {
                leftCastlingMove = OccupiedSquare + Vector2Int.left * 2;
                AvailableMoves.Add(leftCastlingMove);
            }
            rightRook = GetPieceInDirection<Rook>(Team, Vector2Int.right);
            if (rightRook && !rightRook.HasMoved)
            {
                rightCastlingMove = OccupiedSquare + Vector2Int.right * 2;
                AvailableMoves.Add(rightCastlingMove);
            }
        }
    }

    private Piece GetPieceInDirection<T>(TeamColor team, Vector2Int direction)
    {
        for (int i = 1; i <= Board.BOARD_SIZE; i++)
        {
            Vector2Int nextCoords = OccupiedSquare + direction * i;
            Piece piece = Board.GetPieceOnSquare(nextCoords);
            if (!Board.CheckIfCoordinatesAreOnBoard(nextCoords))
                return null;
            if (piece != null)
            {
                if (piece.Team != team || !(piece is T))
                    return null;
                else if (piece.Team == team && piece is T)
                    return piece;
            }
        }
        return null;
    }

    private void AssignStandardMoves()
    {
        float range = 1;
        foreach (var direction in _directions)
        {
            for (int i = 1; i <= range; i++)
            {
                Vector2Int nextCoords = OccupiedSquare + direction * i;
                Piece piece = Board.GetPieceOnSquare(nextCoords);
                if (!Board.CheckIfCoordinatesAreOnBoard(nextCoords))
                    break;
                if (piece == null)
                    TryToAddMove(nextCoords);
                else if (!piece.IsFromSameTeam(this))
                {
                    TryToAddMove(nextCoords);
                    break;
                }
                else if (piece.IsFromSameTeam(this))
                    break;
            }
        }
    }

    public override void MovePiece(Vector2Int coords)
    {
        base.MovePiece(coords);
        if (coords == leftCastlingMove)
        {
            Board.UpdateBoardOnPieceMove(coords + Vector2Int.right, leftRook.OccupiedSquare, leftRook, null);
            leftRook.MovePiece(coords + Vector2Int.right);
        }
        else if (coords == rightCastlingMove)
        {
            Board.UpdateBoardOnPieceMove(coords + Vector2Int.left, rightRook.OccupiedSquare, rightRook, null);
            rightRook.MovePiece(coords + Vector2Int.left);
        }
    }

}
