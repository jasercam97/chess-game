﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : Piece
{
	Vector2Int[] _offsets = new Vector2Int[]
	{
		new Vector2Int(2, 1),
		new Vector2Int(2, -1),
		new Vector2Int(1, 2),
		new Vector2Int(1, -2),
		new Vector2Int(-2, 1),
		new Vector2Int(-2, -1),
		new Vector2Int(-1, 2),
		new Vector2Int(-1, -2),
	};

	public override List<Vector2Int> SelectAvailableSquares()
	{
		AvailableMoves.Clear();

		for (int i = 0; i < _offsets.Length; i++)
		{
			Vector2Int nextCoords = OccupiedSquare + _offsets[i];
			Piece piece = Board.GetPieceOnSquare(nextCoords);
			if (!Board.CheckIfCoordinatesAreOnBoard(nextCoords))
				continue;
			if (piece == null || !piece.IsFromSameTeam(this))
				TryToAddMove(nextCoords);
		}
		return AvailableMoves;
	}
}
