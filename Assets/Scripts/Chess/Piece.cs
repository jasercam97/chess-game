﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MaterialSetter))]
[RequireComponent(typeof(IObjectTweener))]
public abstract class Piece : MonoBehaviour
{
	[SerializeField] private MaterialSetter materialSetter;
	public Board Board { protected get; set; }
	public Vector2Int OccupiedSquare { get; set; }
	public TeamColor Team { get; set; }
	public bool HasMoved { get; private set; }
	public List<Vector2Int> AvailableMoves;

	private IObjectTweener _tweener;

	public abstract List<Vector2Int> SelectAvailableSquares();

	private void Awake()
	{
		AvailableMoves = new List<Vector2Int>();
		_tweener = GetComponent<IObjectTweener>();
		materialSetter = GetComponent<MaterialSetter>();
		HasMoved = false;
	}

	public void SetMaterial(Material selectedMaterial)
	{
		materialSetter.SetSingleMaterial(selectedMaterial);
	}

	public bool IsFromSameTeam(Piece piece)
	{
		return Team == piece.Team;
	}

	public bool CanMoveTo(Vector2Int coords)
	{
		return AvailableMoves.Contains(coords);
	}

	public virtual void MovePiece(Vector2Int coords)
	{
		Vector3 targetPosition = Board.CalculatePositionFromCoords(coords);
		OccupiedSquare = coords;
		HasMoved = true;
		_tweener.MoveTo(transform, targetPosition);
	}


	protected void TryToAddMove(Vector2Int coords)
	{
		AvailableMoves.Add(coords);
	}

	public void SetData(Vector2Int coords, TeamColor team, Board board)
	{
		this.Team = team;
		OccupiedSquare = coords;
		this.Board = board;
		transform.position = board.CalculatePositionFromCoords(coords);
	}

	public bool IsAttackingPieceOfType<T>() where T : Piece
	{
		foreach (var square in AvailableMoves)
		{
			if (Board.GetPieceOnSquare(square) is T)
				return true;
		}
		return false;
	}

}