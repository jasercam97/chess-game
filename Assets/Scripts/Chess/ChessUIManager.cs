using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ChessUIManager : MonoBehaviour
{
    public GameObject PanelCoinsEarned;

    [SerializeField] private GameObject UIParent;
    [SerializeField] private TMP_Text resultText;
    [SerializeField] private Image TeamIndicator;
    [SerializeField] private GameObject JaqueIndicator;

    private void Start()
    {
        JaqueIndicator.SetActive(false);
        TeamIndicator.color = Color.white;
    }

    public void HideUI()
    {
        UIParent.SetActive(false);
    }

    public void OnGameFinished(string winner)
    {
        UIParent.SetActive(true);
        resultText.text = string.Format("{0} won", winner);
        PanelCoinsEarned.SetActive(false);
    }

    public void ActiveCoinsEarnedPanel()
    {
        PanelCoinsEarned.SetActive(true);
    }

    public void ChangeTeamIndicator(TeamColor team)
    {
        if (team == TeamColor.Black)
            TeamIndicator.color = Color.black;
        else
            TeamIndicator.color = Color.white;
    }

    public void ShowJaque(bool state)
    {
        JaqueIndicator.SetActive(state);
    }

}
