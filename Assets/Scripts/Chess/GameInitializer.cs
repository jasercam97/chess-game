using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class GameInitializer : Singleton<GameInitializer>
{
    [Header("Game mode dependent objects")]

    [SerializeField] private LocalModeController localControllerPrefab;
    [SerializeField] private OnlineModeController onlineControllerPrefab;
    [SerializeField] private LocalModeBoard localBoardPrefab;
    [SerializeField] private OnlineModeBoard onlineBoardPrefab;

    [Header("Scene references")]
    //[SerializeField] private NetworkManager networkManager;
    [SerializeField] private CameraSetup cameraSetup;
    [SerializeField] private ChessUIManager uiManager;
    [SerializeField] private Transform boardAnchor;
    private OnlineModeBoard _onlineBoard;
    private bool _isOnline = false;
    private TeamColor _team;

    private void Awake()
    {
        NetworkManager.Instance.SetInit(this);
    }

    public void StartGameMode()
    {
        if (_isOnline) 
        {
            CreateOnlineBoard(); 
            Invoke(nameof(InitializeOnlineController), 0.25f);
            cameraSetup.SetupCamera(_team);
        }
        else 
        {
            CreateLocalBoard(); 
            InitializeLocalController();
        }
    }

    private void CreateOnlineBoard()
    {
        if (PhotonNetwork.IsMasterClient) { PhotonNetwork.Instantiate(onlineBoardPrefab.name, boardAnchor.position, boardAnchor.rotation); }
    }

    private void CreateLocalBoard()
    {
        Instantiate(localBoardPrefab, boardAnchor);
    }

    private void InitializeOnlineController()
    {
        OnlineModeBoard board = FindObjectOfType<OnlineModeBoard>();
        OnlineModeController controller = Instantiate(onlineControllerPrefab);
        controller.SetDependencies(cameraSetup, uiManager, board);
        controller.InitializeGame();
        controller.SetLocalPlayer(_team);
        NetworkManager.Instance.SetDependencies(controller);
        board.SetDependencies(controller);
        controller.StartNewGame();
    }

    private void InitializeLocalController()
    {
        LocalModeBoard board = FindObjectOfType<LocalModeBoard>();
        LocalModeController controller = Instantiate(localControllerPrefab);
        controller.SetDependencies(cameraSetup, uiManager, board);
        controller.InitializeGame();
        board.SetDependencies(controller);
        controller.StartNewGame();
    }

    public void SetGameMode(bool isOnline)
    {
        _isOnline = isOnline;
    }

    public void SetTeam(TeamColor team)
    {
        _team = team;
    }

    public void SetDependencies(ChessUIManager uiManager, Transform anchor, CameraSetup camera) 
    {
        this.uiManager = uiManager;
        this.boardAnchor = anchor;
        this.cameraSetup = camera;
    }

    public bool GetGameMode() { return _isOnline; }
}
