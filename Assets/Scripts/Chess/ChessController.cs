﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

[RequireComponent(typeof(PiecesCreator))]
public abstract class ChessController : Singleton<ChessController>
{
    public enum GameState
    {
        Init, Play, Finished
    }

    [SerializeField] private BoardLayout startingBoardLayout;
    [SerializeField] private Board board;
    [SerializeField] protected ChessUIManager UIManager;

    protected CameraSetup _cameraSetup;

    private PiecesCreator _pieceCreator;
    protected ChessPlayer _whitePlayer;
    protected ChessPlayer _blackPlayer;
    protected ChessPlayer _activePlayer;

    protected GameState _state;

    protected abstract void SetGameState(GameState state);
    public abstract void TryToStartCurrentGame();
    public abstract bool CanPerformMove();

    private void Awake()
    {
        _pieceCreator = GetComponent<PiecesCreator>();
        //CreatePlayers();
    }

    public void SetDependencies(CameraSetup camera, ChessUIManager uiManager, Board board)
    {
        this.UIManager = uiManager;
        this.board = board;
        this._cameraSetup = camera;
    }

    public void InitializeGame()
    {
        CreatePlayers();
    }

    public void CreatePlayers()
    {
        _whitePlayer = new ChessPlayer(TeamColor.White, board);
        _blackPlayer = new ChessPlayer(TeamColor.Black, board);
    }

    public void StartNewGame()
    {
        SetGameState(GameState.Init);
        UIManager.HideUI();
        CreatePiecesFromLayout(startingBoardLayout);
        _activePlayer = _whitePlayer;
        GenerateAllPossiblePlayerMoves(_activePlayer);
        TryToStartCurrentGame();
    }
    
    public void SetupCamera(TeamColor team)
    {
        _cameraSetup.SetupCamera(team);
    }

    internal bool IsGameInProgress()
    {
        return _state == GameState.Play;
    }

    private void CreatePiecesFromLayout(BoardLayout layout)
    {
        for (int i = 0; i < layout.GetPiecesCount(); i++)
        {
            Vector2Int squareCoords = layout.GetSquareCoordsAtIndex(i);
            TeamColor team = layout.GetSquareTeamColorAtIndex(i);
            string typeName = layout.GetSquarePieceNameAtIndex(i);

            Type type = Type.GetType(typeName);
            CreatePieceAndInitialize(squareCoords, team, type);
        }
    }

    public void CreatePieceAndInitialize(Vector2Int squareCoords, TeamColor team, Type type)
    {
        Piece newPiece = _pieceCreator.CreatePiece(type).GetComponent<Piece>();
        newPiece.SetData(squareCoords, team, board);
        
        //Icons
        newPiece.GetComponentInChildren<IconSelector>().CheckIcon(team);

        Material teamMaterial = _pieceCreator.GetTeamMaterial(team);
        newPiece.SetMaterial(teamMaterial);

        board.SetPieceOnBoard(squareCoords, newPiece);

        ChessPlayer currentPlayer = team == TeamColor.White ? _whitePlayer : _blackPlayer;
        currentPlayer.AddPiece(newPiece);
    }

    private void GenerateAllPossiblePlayerMoves(ChessPlayer player)
    {
        player.GenerateAllPossibleMoves();
    }

    public bool IsTeamTurnActive(TeamColor team)
    {
        return _activePlayer.Team == team;
    }

    public void EndTurn()
    {
        GenerateAllPossiblePlayerMoves(_activePlayer);
        GenerateAllPossiblePlayerMoves(GetOpponentToPlayer(_activePlayer));
        if (CheckIfGameIsFinished())
        {
            EndGame();
        }
        else
        {
            ChangeActiveTeam();
        }
    }

    private bool CheckIfGameIsFinished()
    {
        Piece[] kingAttackingPieces = _activePlayer.GetPieceAtackingOppositePieceOfType<King>();
        if (kingAttackingPieces.Length > 0)
        {
            UIManager.ShowJaque(true);
            ChessPlayer oppositePlayer = GetOpponentToPlayer(_activePlayer);
            Piece attackedKing = oppositePlayer.GetPiecesOfType<King>().FirstOrDefault();
            oppositePlayer.RemoveMovesEnablingAttakOnPieceOfType<King>(_activePlayer, attackedKing);

            int avaliableKingMoves = attackedKing.AvailableMoves.Count;
            if (avaliableKingMoves == 0)
            {
                bool canCoverKing = oppositePlayer.CanHidePieceFromAttack<King>(_activePlayer);
                if (!canCoverKing)
                    return true;
            }
        }
        else
        {
            UIManager.ShowJaque(false);
        }
        return false;
    }

    private void EndGame()
    {
        SetGameState(GameState.Finished);
        UIManager.OnGameFinished(_activePlayer.Team.ToString());
        GiveCoins();
    }

    abstract public void GiveCoins();

    public void RestartGame()
    {
        DestroyPieces();
        board.OnGameRestarted();
        _whitePlayer.OnGameRestarted();
        _blackPlayer.OnGameRestarted();
        StartNewGame();
    }

    private void DestroyPieces()
    {
        _whitePlayer.ActivePieces.ForEach(p => Destroy(p.gameObject));
        _blackPlayer.ActivePieces.ForEach(p => Destroy(p.gameObject));
    }

    private void ChangeActiveTeam()
    {
        _activePlayer = _activePlayer == _whitePlayer ? _blackPlayer : _whitePlayer;
        UIManager.ChangeTeamIndicator(_activePlayer.Team);
        ChangeCamera();
    }

    public abstract void ChangeCamera();

    private ChessPlayer GetOpponentToPlayer(ChessPlayer player)
    {
        return player == _whitePlayer ? _blackPlayer : _whitePlayer;
    }

    internal void OnPieceRemoved(Piece piece)
    {
        ChessPlayer pieceOwner = (piece.Team == TeamColor.White) ? _whitePlayer : _blackPlayer;
        pieceOwner.RemovePiece(piece);

        //if(SkinsManager.Instance.GetDestroyFX())
        //    Instantiate(SkinsManager.Instance.GetDestroyFX(), piece.transform.position, Quaternion.identity);
    }

    internal void RemoveMovesEnablingAttakOnPieceOfType<T>(Piece piece) where T : Piece
    {
        _activePlayer.RemoveMovesEnablingAttakOnPieceOfType<T>(GetOpponentToPlayer(_activePlayer), piece);
    }
}

