﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SquareSelectorCreator))]
public abstract class Board : MonoBehaviour
{
    public const int BOARD_SIZE = 8;

    [SerializeField] private Transform startingTransform;
    [SerializeField] private float squareSize;

    private Piece[,] _grid;

    [SerializeField]private Piece _selectedPiece;
    private ChessController _chessController;
    private SquareSelectorCreator _squareSelector;

    public abstract void SelectedPieceMoved(Vector2 coords);
    public abstract void SetSelectedPiece(Vector2 coords);


    protected virtual void Awake()
    {
        _squareSelector = GetComponent<SquareSelectorCreator>();
        CreateGrid();
    }

    public void SetDependencies(ChessController chessController)
    {
        this._chessController = chessController;
    }

    private void CreateGrid()
    {
        _grid = new Piece[BOARD_SIZE, BOARD_SIZE];
    }

    public Vector3 CalculatePositionFromCoords(Vector2Int coords)
    {
        return startingTransform.position + new Vector3(coords.x * squareSize, 0f, coords.y * squareSize);
    }

    private Vector2Int CalculateCoordsFromPosition(Vector3 inputPosition)
    {
        int x = Mathf.FloorToInt(transform.InverseTransformPoint(inputPosition).x / squareSize) + BOARD_SIZE / 2;
        int y = Mathf.FloorToInt(transform.InverseTransformPoint(inputPosition).z / squareSize) + BOARD_SIZE / 2;
        return new Vector2Int(x, y);
    }

    public void OnSquareSelected(Vector3 inputPosition)
    {
        if (!_chessController.CanPerformMove())
            return;
        //else { Debug.Log("Move allowed"); }
     
        Vector2Int coords = CalculateCoordsFromPosition(inputPosition);
        Piece piece = GetPieceOnSquare(coords);
        if (_selectedPiece)
        {
            if (piece != null && _selectedPiece == piece)
                DeselectPiece();
            else if (piece != null && _selectedPiece != piece && _chessController.IsTeamTurnActive(piece.Team))
                SelectPiece(coords);
            else if (_selectedPiece.CanMoveTo(coords))
                SelectedPieceMoved(coords);
        }
        else
        {
            if (piece != null && _chessController.IsTeamTurnActive(piece.Team))
                SelectPiece(coords);
        }
    }


    private void SelectPiece(Vector2Int coords)
    {
        Piece piece = GetPieceOnSquare(coords);
        _chessController.RemoveMovesEnablingAttakOnPieceOfType<King>(piece);
        SetSelectedPiece(coords);
        List<Vector2Int> selection = _selectedPiece.AvailableMoves;
        ShowSelectionSquares(selection);
    }

    private void ShowSelectionSquares(List<Vector2Int> selection)
    {
        Dictionary<Vector3, bool> squaresData = new Dictionary<Vector3, bool>();
        for (int i = 0; i < selection.Count; i++)
        {
            Vector3 position = CalculatePositionFromCoords(selection[i]);
            bool isSquareFree = GetPieceOnSquare(selection[i]) == null;
            squaresData.Add(position, isSquareFree);
        }
        _squareSelector.ShowSelection(squaresData);
    }

    private void DeselectPiece()
    {
        _selectedPiece = null;
        _squareSelector.ClearSelection();
    }

    internal void OnSelectedPieceMoved(Vector2Int coords)
    {
        Piece destroying = TryToTakeOppositePiece(coords);
        UpdateBoardOnPieceMove(coords, _selectedPiece.OccupiedSquare, _selectedPiece, null);
        SaveMovement(_selectedPiece.OccupiedSquare, coords, _selectedPiece, destroying);
        _selectedPiece.MovePiece(coords);
        DeselectPiece();
        EndTurn();
    }

    private void SaveMovement(Vector2Int StartPos, Vector2Int EndPos, Piece MovingPiece, Piece Destroying)
    {
        LogClass LogMovement = new LogClass();
        LogMovement.StartPos = StartPos;
        LogMovement.EndPos = EndPos;
        LogMovement.MovingPiece = MovingPiece;
        LogMovement.DestroyePiece = Destroying;
        LogManager.Instance.AddLogMovement(LogMovement);
    }

    internal void OnSetSelectedPiece(Vector2Int coords)
    {
        Piece piece = GetPieceOnSquare(coords);  
        _selectedPiece = piece;
    }

    private void EndTurn()
    {
        _chessController.EndTurn();
    }

    public void UpdateBoardOnPieceMove(Vector2Int newCoords, Vector2Int oldCoords, Piece newPiece, Piece oldPiece)
    {
        _grid[oldCoords.x, oldCoords.y] = oldPiece;
        _grid[newCoords.x, newCoords.y] = newPiece;
    }

    public Piece GetPieceOnSquare(Vector2Int coords)
    {
        if (CheckIfCoordinatesAreOnBoard(coords))
            return _grid[coords.x, coords.y];
        return null;
    }

    public bool CheckIfCoordinatesAreOnBoard(Vector2Int coords)
    {
        if (coords.x < 0 || coords.y < 0 || coords.x >= BOARD_SIZE || coords.y >= BOARD_SIZE)
            return false;
        return true;
    }

    public bool HasPiece(Piece piece)
    {
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                if (_grid[i, j] == piece)
                    return true;
            }
        }
        return false;
    }

    public void SetPieceOnBoard(Vector2Int coords, Piece piece)
    {
        if (CheckIfCoordinatesAreOnBoard(coords)) {
            _grid[coords.x, coords.y] = piece;
        }
    }

    private Piece TryToTakeOppositePiece(Vector2Int coords)
    {
        Piece piece = GetPieceOnSquare(coords);
        if (piece && !_selectedPiece.IsFromSameTeam(piece))
        {
            TakePiece(piece);
            return piece;
        }
        return null;
    }

    private void TakePiece(Piece piece)
    {
        if (piece)
        {
            _grid[piece.OccupiedSquare.x, piece.OccupiedSquare.y] = null;
            _chessController.OnPieceRemoved(piece);

            if (SkinsManager.Instance.GetDestroyFX())
                Instantiate(SkinsManager.Instance.GetDestroyFX(), piece.transform.position, Quaternion.identity);

            Destroy(piece.gameObject, 1f);
        }
    }


    public void PromotePiece(Piece piece)
    {
        TakePiece(piece);
        _chessController.CreatePieceAndInitialize(piece.OccupiedSquare, piece.Team, typeof(Queen));
    }

    internal void OnGameRestarted()
    {
        _selectedPiece = null;
        CreateGrid();
    }

    [ContextMenu("Grid")]
    public void CheckGrid()
    {
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                var v = new Vector2Int(i, j);
                Debug.Log(GetPieceOnSquare(v));
            }
        }
    }

}
