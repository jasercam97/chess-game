using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalModeController : ChessController
{
    public override bool CanPerformMove()
    {
        if (!IsGameInProgress()) { return false; }

        return true;
    }

    public override void ChangeCamera()
    {
        _cameraSetup.FlipCamera();
    }

    public override void GiveCoins()
    {
        Debug.Log("No hay monedas para los solitarios");
    }

    public override void TryToStartCurrentGame()
    {
        SetGameState(GameState.Play);
    }

    protected override void SetGameState(GameState state)
    {
        this._state = state;
    }
}
