﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChessPlayer
{
	public TeamColor Team { get; set; }
	public Board Board { get; set; }
	public List<Piece> ActivePieces { get; private set; }

	public ChessPlayer(TeamColor team, Board board)
	{
		ActivePieces = new List<Piece>();
		this.Board = board;
		this.Team = team;
	}

	public void AddPiece(Piece piece)
	{
		if (!ActivePieces.Contains(piece))
			ActivePieces.Add(piece);
	}

	public void RemovePiece(Piece piece)
	{
		if (ActivePieces.Contains(piece))
			ActivePieces.Remove(piece);
	}

	public void GenerateAllPossibleMoves()
	{
		foreach (var piece in ActivePieces)
		{
			if(Board.HasPiece(piece))
				piece.SelectAvailableSquares();
		}
	}

	public Piece[] GetPieceAtackingOppositePieceOfType<T>() where T : Piece
	{
		return ActivePieces.Where(p => p.IsAttackingPieceOfType<T>()).ToArray();
	}

	public Piece[] GetPiecesOfType<T>() where T : Piece
	{
		return ActivePieces.Where(p => p is T).ToArray();
	}

	public void RemoveMovesEnablingAttakOnPieceOfType<T>(ChessPlayer opponent, Piece selectedPiece) where T : Piece
	{
		List<Vector2Int> coordsToRemove = new List<Vector2Int>();

		coordsToRemove.Clear();
		foreach (var coords in selectedPiece.AvailableMoves)
		{
			Piece pieceOnCoords = Board.GetPieceOnSquare(coords);
			Board.UpdateBoardOnPieceMove(coords, selectedPiece.OccupiedSquare, selectedPiece, null);
			opponent.GenerateAllPossibleMoves();
			if (opponent.CheckIfIsAttackingPiece<T>())
				coordsToRemove.Add(coords);
			Board.UpdateBoardOnPieceMove(selectedPiece.OccupiedSquare, coords, selectedPiece, pieceOnCoords);
		}
		foreach (var coords in coordsToRemove)
		{
			selectedPiece.AvailableMoves.Remove(coords);
		}

	}

	internal bool CheckIfIsAttackingPiece<T>() where T : Piece
	{
		foreach (var piece in ActivePieces)
		{
			if (Board.HasPiece(piece) && piece.IsAttackingPieceOfType<T>())
				return true;
		}
		return false;
	}

	public bool CanHidePieceFromAttack<T>(ChessPlayer opponent) where T : Piece
	{
		foreach (var piece in ActivePieces)
		{
			foreach (var coords in piece.AvailableMoves)
			{
				Piece pieceOnCoords = Board.GetPieceOnSquare(coords);
				Board.UpdateBoardOnPieceMove(coords, piece.OccupiedSquare, piece, null);
				opponent.GenerateAllPossibleMoves();
				if (!opponent.CheckIfIsAttackingPiece<T>())
				{
					Board.UpdateBoardOnPieceMove(piece.OccupiedSquare, coords, piece, pieceOnCoords);
					return true;
				}
				Board.UpdateBoardOnPieceMove(piece.OccupiedSquare, coords, piece, pieceOnCoords);
			}
		}
		return false;
	}

	internal void OnGameRestarted()
	{
		ActivePieces.Clear();
	}



}