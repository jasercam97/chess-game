using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using PlayfabManagerName;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineModeController : ChessController, IOnEventCallback
{
    protected const byte SET_GAME_STATE_EVENT_CODE = 1;

    private ChessPlayer _localPlayer;

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public override bool CanPerformMove()
    {
        if (!IsGameInProgress() || !IsLocalPlayerTurn())
            return false;
        return true;
    }

    public bool IsLocalPlayerTurn()
    {
        return _localPlayer == _activePlayer;
    }

    public void SetLocalPlayer(TeamColor team)
    {
        _localPlayer = team == TeamColor.White ? _whitePlayer : _blackPlayer;
    }

    public override void TryToStartCurrentGame()
    {
        SetGameState(GameState.Play);
    }

    protected override void SetGameState(GameState state)
    {
        object[] content = new object[] { (int)state };
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(SET_GAME_STATE_EVENT_CODE, content, raiseEventOptions, SendOptions.SendReliable);
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;
        if(eventCode == SET_GAME_STATE_EVENT_CODE)
        {
            object[] data = (object[])photonEvent.CustomData;
            GameState state = (GameState)data[0];
            this._state = state;
        }
    }

    public override void GiveCoins()
    {
        if (GameInitializer.Instance.GetGameMode() && _activePlayer == _localPlayer)
        {
            PlayfabManager.Instance.EarnMoney(1);
            PlayfabManager.Instance.AddPoints(UnityEngine.Random.Range(35, 55));
            
            UIManager.ActiveCoinsEarnedPanel();
        }
        else
        {
            PlayfabManager.Instance.AddPoints(UnityEngine.Random.Range(-15, -5));
        }
    }

    public override void ChangeCamera()
    {
        // Panel grisaceo para indicar que no es su turno(?) ***
    }
}
