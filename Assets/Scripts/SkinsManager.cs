using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinsManager : MonoBehaviour
{
    public static SkinsManager Instance;
    [Header("Reference")]
    public SkinSaver Saver;

    [Header("Materiales")]
    [SerializeField] private Material WhiteMaterial;
    [SerializeField] private Material BlackMaterial;
    [SerializeField] private Material BoardMaterial;
    [SerializeField] private GameObject DestroyEffect;

    private void Awake()
    {
        if(Instance == null)
            Instance = this;

    }

    #region SETTER
    public void SetWhiteMat(Material Mat)
    {
        WhiteMaterial = Mat;
    }

    public void SetBlackMat(Material Mat)
    {
        BlackMaterial = Mat;
    }

    public void SetBoardMat(Material Mat)
    {
        BoardMaterial = Mat;
    }

    public void SetDestroyFX(GameObject Prefab)
    {
        DestroyEffect = Prefab;
    }

    #endregion

    #region GETTER

    public Material GetWhiteMat()
    {
        return WhiteMaterial;
    }

    public Material GetBlackMat()
    {
        return BlackMaterial;
    }

    public Material GetBoardMat()
    {
        return BoardMaterial;
    }

    public GameObject GetDestroyFX()
    {
        return DestroyEffect;
    }
    #endregion

    #region CHECKER
    public bool CheckWhiteMat(Material Mat)
    {
        return Mat == WhiteMaterial;
    }

    public bool CheckBlackMat(Material Mat)
    {
        return Mat == BlackMaterial;
    }

    public bool CheckBoardMat(Material Mat)
    {
        return Mat == BoardMaterial;
    }

    public bool CheckDestroyFX(GameObject Prefab)
    {
        return Prefab == DestroyEffect;
    }

    #endregion

    public void Save()
    {
        Saver.SaveMaterials();
    }
}
